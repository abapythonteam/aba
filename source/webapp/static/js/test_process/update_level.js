$(document).on('click', '.press_test', function () {
    $.ajax({
        url: $(this).data('url'),
    }).done((body) => {
        status = body.status;
        if (status === 'false') {
            $(this).html('Открыть тест');
            $(this).removeClass('btn-danger');
            $(this).addClass('btn-success');
        }
        if (status === 'true') {
            $(this).html('Завершить тест');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-danger');
        }
    });
});

$(document).on('click', '.press_status', function () {
    const form_id = $(this).data('form_id');
    $.ajax({
        url: $(this).data('url'),
    }).done((body) => {
        status = body.status;
        if (status === 'false') {
            $(this).html('X');
            $(this).addClass('on');
            $(`#${form_id} div`).removeClass('press_level');
        }
        if (status === 'true') {
            $(this).html('&nbsp;');
            $(this).removeClass('on');
            $(`#${form_id} div`).addClass('press_level');
        }
    });
});

$(document).on('click', '.press_level', function () {
    const formId = $(this).data('form_id');
    let value = $(this).data('value');
    $(`#${formId} input[name="value"]`).attr('value', value);
    $.ajax({
        url: $('#' + formId).attr('action'),
        data: $('#' + formId).serialize(),
        type: 'POST',
    }).done((body) => {
        value = body.level;
        const btns = $('#' + formId + ' div').get();
        eqal_enable(btns, value);
    });
});

function eqal_enable(list, value) {
    list.forEach((btn) => {
        const current_value = $(btn).data('value');
        if (value >= current_value) {
            $(btn).addClass('on');
        }
        if (value < current_value) {
            $(btn).removeClass('on');
        }
    })
}