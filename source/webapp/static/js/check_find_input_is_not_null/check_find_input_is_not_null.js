(() => {
    $('.btn-disabled').prop('disabled', true);
    $('.form-control').keyup(function () {
        ($('#field').val()) ? $('.btn-disabled').prop('disabled', false) :
            $('.btn-disabled').prop('disabled', true);
    });
    (!$(".open_program").text()) ? $("#no_open_program").text('Нет открытых программ') : null;
    (!$(".close_program").text()) ? $("#no_close_program").text('Нет закрытых программ') : null;
})();