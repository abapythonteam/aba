from django.urls import path
from webapp.MySQL_to_Word.homework_word import create_word_homework
from webapp.MySQL_to_Word.program_word import create_word_program
from webapp.views.skils import SkillDetailView, SkillUpdateView, SkillCreateView, SkillSearchView, delete_skill
from webapp.views.child import ChildList, ChildSearchView, ChildDetailView, ChildUpdateView, ChildCreateView, \
    soft_delete_child
from webapp.views.program import ProgramDetailView, ProgramListView, ProgramCreateView, ProgramUpdateView, \
    ProgramSearchView, AddSkillInProgram, SkillInProgramUpdate, soft_delete_program
from webapp.views.categories import CategoriesListView, CategoriesDetailView, CategoriesCreateView, \
    CategoriesUpdateView, CategoriesSearchView
from webapp.views.session import create_session_and_result, counter_get_view, counter_done, \
    counter_done_with_hint, cancel_session
from webapp.views.result import change_status_skill, ResultListView, close_session_result_view
from webapp.views.main_page import MainListView
from webapp.views.users import register, UserDetailView, UserListView, user_update, soft_delete_user, \
    user_password_update
from webapp.MySQL_to_Excel.sheet import create_excel_file
from webapp.views.testing import CreateTesting, StartTesting, update_result_level, update_result_status, \
    update_test_status
from webapp.views.error import error_view, instruction

app_name = 'webapp'

urlpatterns = [
    path('', MainListView.as_view(), name='main_page'),
    path('access_denied/', error_view, name='error_page'),
    path('instruction/', instruction, name='instruction'),
    # users-url -----------------------------------------------------------------------------------------------
    path('register/', register, name='register'),
    path('users_list/', UserListView.as_view(), name='users_list'),
    path('user/<int:pk>/detail/', UserDetailView.as_view(), name='user_detail'),
    path('user/<int:u_pk>/update/', user_update, name='user_update'),
    path('user/<int:pk>/delete/', soft_delete_user, name='user_delete'),
    path('user/<int:pk>/password_change/', user_password_update, name='password_update'),
    # counter urls------------------------------------------------------------------------------------------------
    path('skill_w_hint/<int:pk>', counter_done_with_hint, name='done_w_hint'),
    path('skill_done/<int:pk>', counter_done, name='counter_done'),
    path('get_skills/<int:pk>', counter_get_view, name='counter_get_view'),
    # child urls--------------------------------------------------------------------------------------------------
    path('child/', ChildList.as_view(), name='child_list'),
    path('child_search/', ChildSearchView.as_view(), name='search_view'),
    path('child/<int:pk>', ChildDetailView.as_view(), name='child_detail'),
    path('child/create', ChildCreateView.as_view(), name='child_create'),
    path('child/<int:pk>/update', ChildUpdateView.as_view(), name='child_update'),
    path('child/<int:pk>/delete', soft_delete_child, name='child_delete'),
    # program urls------------------------------------------------------------------------------------------------
    path('program/', ProgramListView.as_view(), name='program_list'),
    path('program_search/', ProgramSearchView.as_view(), name='search_view_program'),
    path('program/<int:pk>', ProgramDetailView.as_view(), name='program_detail'),
    path('program/create', ProgramCreateView.as_view(), name='program_create'),
    path('program/<int:pk>/update', ProgramUpdateView.as_view(), name='program_update'),
    path('program/<int:pk>/create', AddSkillInProgram.as_view(), name='program_skill_add'),
    path('program/<int:program_pk>/skill/<int:pk>/update/', SkillInProgramUpdate.as_view(),
         name='program_skill_update'),
    path('program/<int:pk>/delete', soft_delete_program, name='program_delete'),
    # category urls-----------------------------------------------------------------------------------------------
    path('categories/', CategoriesListView.as_view(), name='categories_list'),
    path('categories/<int:pk>', CategoriesDetailView.as_view(), name='categories_detail'),
    path('categories/create', CategoriesCreateView.as_view(), name='categories_create'),
    path('categories/<int:pk>/update', CategoriesUpdateView.as_view(), name='categories_update'),
    path('categories/categories_search/', CategoriesSearchView.as_view(), name='search_view_categories'),
    # session_urls------------------------------------------------------------------------------------------------
    path('program/<int:pk>/session', create_session_and_result, name='session_create'),
    # path('program/<int:pk>/skill/create', AddExtraSkill.as_view(), name='extra_skill'),
    path('session/<int:pk>/cancel', cancel_session, name='cancel_session'),
    # result_urls------------------------------------------------------------------------------------------------
    path('session/<int:pk>/result', ResultListView.as_view(), name='session_result_view'),
    path('session/<int:pk>', close_session_result_view, name='change_status_session'),
    path('skill_in_program/<int:pk>', change_status_skill, name='change_status_skill'),
    # skill urls--------------------------------------------------------------------------------------------------
    path('skill/<int:pk>', SkillDetailView.as_view(), name='skill_detail'),
    path('skill/create', SkillCreateView.as_view(), name='skill_create'),
    path('skill/<int:pk>/update', SkillUpdateView.as_view(), name='skill_update'),
    path('skill/<int:pk>/delete', delete_skill, name='skill_delete'),
    path('skill/skill_search/', SkillSearchView.as_view(), name='search_view_skill'),
    path('skill/skill_search/', SkillSearchView.as_view(), name='search_view_skill'),
    # testing urls--------------------------------------------------------------------------------------------------
    path('child/<int:child_pk>/create_test', CreateTesting.as_view(), name='create_testing'),
    path('child/<int:child_pk>/test/<int:test_pk>', StartTesting.as_view(), name='start_testing'),
    path('test/<int:pk>', update_result_level, name='update_level'),
    path('change_result_status/<int:pk>', update_result_status, name='update_result_status'),
    path('change_test_status/<int:pk>', update_test_status, name='update_test_status'),
    # import word_and_excel---------------------------------------------------------------------------------------------
    path('create_excel/<int:program_pk>', create_excel_file, name='create_excel'),
    path('create_homework/<int:program_pk>', create_word_homework, name='create_homework'),
    path('create_program/<int:program_pk>', create_word_program, name='create_program'),
]
