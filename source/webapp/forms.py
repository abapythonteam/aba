import datetime
import re
from django import forms
from django.contrib.auth.models import User, Group
from webapp.models import Child, UserInfo, Result, Skill, Program, Categories, SkillsInProgram, StudyMethod, \
    HintTypeDelete, HintType, UsersChild
from django.contrib.auth.forms import UserCreationForm, AdminPasswordChangeForm
from django_select2.forms import Select2MultipleWidget
from django.contrib.auth import password_validation


class PasswordChangeForm(AdminPasswordChangeForm):
    error_css_class = 'text-danger small font-weight-light d-block'
    required_css_class = 'required'
    password1 = forms.CharField(
        label='Пароль',
        strip=False,
        widget=forms.PasswordInput(attrs={'autofocus': True, 'class': 'form-control form-control-sm shadow-none',
                                          'placeholder': 'Введите новый пароль'}))

    password2 = forms.CharField(
        label='Подтвердите пароль',
        strip=False,
        widget=forms.PasswordInput(
            attrs={'class': 'form-control form-control-sm shadow-none', 'placeholder': 'Подтвердите пароль'}))

    def clean_password1(self):
        password1 = self.cleaned_data['password1']
        min_len = 8
        if len(password1) < min_len:
            raise forms.ValidationError('Минимальная длинна 8 символов')
        password_validation.validate_password(password1, self.user)
        return password1


class UserUpdateForm(forms.ModelForm):
    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(),
                                            widget=Select2MultipleWidget
                                            (attrs={'class': 'form-control form-control-sm shadow-none'}),
                                            required=False)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'groups', 'email']
        widgets = {
            'username': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Логин пользователя'}),
            'first_name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Имя пользователя'}),
            'last_name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Фамилия пользователя'}),
            'email': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Почта пользователя'})

        }


class ProfileUpdateForm(forms.ModelForm):
    child = forms.ModelMultipleChoiceField(queryset=Child.objects.all(),
                                           widget=Select2MultipleWidget(
                                               attrs={'class': 'form-control form-control-sm'}),
                                           required=False)

    class Meta:
        model = UserInfo
        fields = ['phone', 'child']
        widgets = {
            'phone': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Логин пользователя'}),
        }

    def __init__(self, *args, **kwargs):
        super(ProfileUpdateForm, self).__init__(*args, **kwargs)
        self.fields['child'].queryset = Child.objects.filter(is_deleted=False)


class UserForm(UserCreationForm):
    error_css_class = 'text-danger small font-weight-light d-block'
    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(),
                                            widget=Select2MultipleWidget
                                            (attrs={'class': 'form-control form-control-sm shadow-none'}),
                                            required=False)

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'email', 'first_name', 'last_name', 'groups']
        widgets = {
            'username': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Логин пользователя'}),
            'password1': forms.PasswordInput(
                attrs={'style': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Пароль'}),
            'password2': forms.PasswordInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Подтверждение пароля'}),
            'first_name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Имя'}),
            'last_name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Фамилия'}),
            'email': forms.EmailInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Почта'}
            )
        }

    def clean_password1(self):
        password1 = self.cleaned_data['password1']
        if len(password1) < 8:
            raise forms.ValidationError('Пароль должен быть не меньше 8 символов')


class UserInfoForm(forms.ModelForm):
    child = forms.ModelMultipleChoiceField(queryset=Child.objects.all(),
                                           widget=Select2MultipleWidget
                                           (attrs={'class': 'form-control form-control-sm shadow-none'}),
                                           required=False)

    class Meta:
        model = UserInfo
        fields = ['phone', 'child']
        widgets = {
            'phone': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Контакты'}),
        }

    def __init__(self, *args, **kwargs):
        super(UserInfoForm, self).__init__(*args, **kwargs)
        self.fields['child'].queryset = Child.objects.filter(is_deleted=False)


class SkillForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Categories.objects.all(),
                                      empty_label='Не выбрано...',
                                      widget=forms.Select
                                      (attrs={'class': 'form-control form-control-sm shadow-none', 'required': True}),
                                      label='Секция', )

    difficulty = forms.IntegerField(max_value=200000, min_value=0)

    class Meta:
        model = Skill
        exclude = ['created_date', 'updated_date', 'deleted_date']
        widgets = {
            'name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none is-valid',
                       'placeholder': 'Название навыка', 'required': True}),
            'code_skill': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none is-valid',
                       'placeholder': 'Код навыка', 'required': True}),
            'skill_comment': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Комментарий к навыку'}),
            'description': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Описание навыка'}),
            'criterion': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Критерий навыка'}),
        }


class ChildForm(forms.ModelForm):
    class Meta:
        model = Child
        exclude = ["created_date", "edited_date", "deleted_date", "is_deleted"]
        widgets = {
            'first_name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none is-valid',
                       'placeholder': 'Имя ребенка', 'required': True}),
            'last_name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none is-valid',
                       'placeholder': 'Фамилия ребенка', 'required': True}),
            'third_name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Отчество ребенка'}),
            'birthday': forms.SelectDateWidget(empty_label=('Год', 'Месяц', 'День'),
                                               years=[r for r in range(1970, datetime.date.today().year + 1)],
                                               attrs={
                                                   'class': 'form-control form-control-sm '
                                                            'shadow-none w-25 d-inline-block',
                                                   'placeholder': 'Дата рождения', }),
            'age': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none is-valid', 'placeholder': 'Возраст',
                       'type': 'number', 'required': True}),
            'address': forms.TextInput(attrs={'class': 'form-control form-control-sm shadow-none',
                                              'placeholder': 'Адрес'}),
            'characteristic': forms.Textarea(attrs={'class': 'form-control form-control-sm shadow-none',
                                                    'placeholder': 'Характеристика', 'rows': '3'}),
            'preferences': forms.Textarea(attrs={'class': 'form-control form-control-sm shadow-none',
                                                 'placeholder': 'Предпочтения', 'rows': '3'}),
            'communication_system': forms.Textarea(attrs={'class': 'form-control form-control-sm shadow-none',
                                                          'placeholder': 'Система коммуникации', 'rows': '3'}),
            'first_parent': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none is-valid',
                       'placeholder': 'Первый родитель', 'required': True}),
            'second_parent': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Второй родитель'}),
            'contacts': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Контакты'}),
        }


class ResultForm(forms.ModelForm):
    class Meta:
        model = Result
        fields = ["done", "done_with_hint"]


class ProgramCreateForm(forms.ModelForm):
    child = forms.ModelChoiceField(queryset=Child.objects.all(),
                                   empty_label='Не выбрано...',
                                   widget=forms.Select
                                   (attrs={'class': 'form-control form-control-sm shadow-none'}),
                                   label='Ребёнок', required=True)

    class Meta:
        model = Program
        exclude = ["created_date", "edited_date", "deleted_date", "status", "author_therapist", "skills"]
        widgets = {
            'name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Программа'}),
            'description': forms.Textarea(attrs={'class': 'form-control form-control-sm shadow-none',
                                                 'placeholder': 'Описание программы', 'rows': '3'}),
            'program_comment': forms.Textarea(attrs={'class': 'form-control form-control-sm shadow-none',
                                                     'placeholder': 'Комментарий к программе', 'rows': '3'}),
        }

    def __init__(self, *args, **kwargs):
        user_pk = kwargs.pop('user_pk')
        user = kwargs.pop('user_is_admin')
        group = kwargs.pop('group')
        if (not user) and not str(group) == "ACD":
            super(ProgramCreateForm, self).__init__(*args, **kwargs)
            now_user = UserInfo.objects.get(id=user_pk)
            user_child = UsersChild.objects.filter(user_id=now_user.pk)
            childs_pk = re.findall(r'\d+', str(user_child.values('child_id')))
            self.fields['child'].queryset = Child.objects.filter(id__in=list(map(int, childs_pk)), is_deleted=False)
        else:
            super(ProgramCreateForm, self).__init__(*args, **kwargs)
            self.fields['child'].queryset = Child.objects.filter(is_deleted=False)


class ProgramForm(forms.ModelForm):
    skills = forms.ModelMultipleChoiceField(queryset=Skill.objects.all(),
                                            widget=Select2MultipleWidget(
                                                attrs={'class': 'form-control form-control-sm'}),
                                            label='Навыки', required=False)

    class Meta:
        model = Program
        exclude = ["created_date", "edited_date", "deleted_date", "status", "author_therapist", "child"]
        widgets = {
            'name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Программа'}),
            'description': forms.Textarea(attrs={'class': 'form-control form-control-sm shadow-none',
                                                 'placeholder': 'Описание программы', 'rows': '3'}),
            'program_comment': forms.Textarea(attrs={'class': 'form-control form-control-sm shadow-none',
                                                     'placeholder': 'Комментарий к программе', 'rows': '3'}),
        }


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Categories
        exclude = ["created_date", "edited_date", "deleted_date"]
        widgets = {
            'name': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none is-valid',
                       'placeholder': 'Название секции', 'required': True}),
            'code_category': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Код секции', 'required': True}),
        }


class SkillsInProgramForm(forms.ModelForm):
    skill = forms.ModelChoiceField(queryset=Skill.objects.all(),
                                   empty_label='Не выбрано...',
                                   widget=forms.Select
                                   (attrs={'class': 'form-control form-control-sm shadow-none'}),
                                   label='Навык', required=True)
    study_method = forms.ModelChoiceField(queryset=StudyMethod.objects.all(),
                                          empty_label='Не выбрано...',
                                          widget=forms.Select
                                          (attrs={'class': 'form-control form-control-sm shadow-none'}),
                                          label='Метод обучения', required=False)
    hint_type = forms.ModelChoiceField(queryset=HintType.objects.all(),
                                       empty_label='Не выбрано...',
                                       widget=forms.Select
                                       (attrs={'class': 'form-control form-control-sm shadow-none'}),
                                       label='Вид подсказки', required=False)
    hint_type_delete = forms.ModelChoiceField(queryset=HintTypeDelete.objects.all(),
                                              empty_label='Не выбрано...',
                                              widget=forms.Select
                                              (attrs={'class': 'form-control form-control-sm shadow-none', }),
                                              label='Вид удаления подсказки', required=False)

    class Meta:
        model = SkillsInProgram
        exclude = ["program", "status"]
        widgets = {
            'stimulus': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none w-100',
                       'placeholder': 'Стимул отработки'}),
            'reinforcement_reaction': forms.Textarea(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Метод обучения', 'rows': '2'}),
            'reinforcement_mode': forms.Textarea(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Метод обучения', 'rows': '2'}),
            'hint_levels': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Уровень подсказки'}),
            'generalization': forms.Textarea(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Метод обучения', 'rows': '2'}),
            'added_skill': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Добавить навык'}),
            'added_skill_comment': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Комментарий к добавленному навыку'}),
            'mastery_criterion': forms.TextInput(
                attrs={'class': 'form-control form-control-sm shadow-none',
                       'placeholder': 'Критерий мастерства'}),
        }
