from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect, render
from webapp.models import Skill
from django.views.generic import DetailView, CreateView, UpdateView, View
from webapp.forms import SkillForm
from django.urls import reverse
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


class SkillDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Skill
    template_name = 'skill_views/skill_detail.html'
    permission_required = 'webapp.view_skill'


class SkillCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Skill
    form_class = SkillForm
    template_name = 'skill_views/skill_create.html'
    permission_required = 'webapp.add_skill'

    def get_success_url(self):
        return reverse('webapp:skill_detail', kwargs={'pk': self.object.pk})


class SkillUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Skill
    form_class = SkillForm
    template_name = 'skill_views/skill_update.html'
    permission_required = 'webapp.change_skill'

    def get_success_url(self):
        return reverse('webapp:skill_detail', kwargs={'pk': self.object.pk})


def delete_skill(request, pk):
    if not request.user.has_perm('webapp.delete_skill'):
        raise PermissionDenied("You don't have permission to delete child")
    else:
        skill = get_object_or_404(Skill, pk=pk)
        skill.delete()
        return redirect('webapp:categories_list')


class SkillSearchView(LoginRequiredMixin, View):
    template_name = 'skill_views/skill_search_results.html'

    def get(self, request):
        query = self.request.GET.get('q')
        searched_skills = Skill.objects.filter(
            Q(name__icontains=query) |
            Q(code_skill__icontains=query))
        context = {
            'searched_skills': searched_skills
        }
        return render(self.request, self.template_name, context)
