from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.contrib.auth.models import User
from webapp.models import UserInfo
from django.shortcuts import redirect
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
from webapp.forms import UserForm, UserInfoForm, UserUpdateForm, ProfileUpdateForm, PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


def user_password_update(request, pk):
    if not request.user.has_perm('webapp.can_register'):
        raise PermissionDenied("You don't have permission!")
    else:
        account = User.objects.get(pk=pk)
        if request.method == 'POST':
            form = PasswordChangeForm(account, request.POST)
            if form.is_valid():
                form.save(commit=True)
                username = account.username
                messages.success(request, f'Пароль пользователя "{username}" успешно обновлен')
                return redirect('webapp:user_detail', pk=account.client.pk)
            else:
                messages.info(request, f'Ошибка смены пароля пользователя "{account.username}"')
                return redirect('webapp:password_update', pk=pk)
        else:
            form = PasswordChangeForm(account)
        context = {
            'form': form,
            'account': account
        }
        return render(request, 'users/user_password_update.html', context)


def user_update(request, u_pk):
    if not request.user.has_perm('webapp.can_register'):
        raise PermissionDenied("You don't have permission!")
    else:
        if request.method == 'POST':
            u_form = UserUpdateForm(request.POST, instance=User.objects.get(pk=u_pk))
            p_form = ProfileUpdateForm(request.POST, instance=UserInfo.objects.get(user_id=u_pk))
            if u_form.is_valid() and p_form.is_valid():
                user = u_form.save()
                profile = p_form.save()
                profile.user_id = user.pk
                child = p_form.cleaned_data.get('child')
                for i in child:
                    profile.child.add(i)
                user.save()
                profile.save()
                username = u_form.cleaned_data['username']
                messages.success(request, f'Профиль "{username}" успешно обновлен')
                return redirect('webapp:user_detail', pk=UserInfo.objects.get(user_id=u_pk).pk)
        else:
            u_form = UserUpdateForm(instance=User.objects.get(pk=u_pk))
            p_form = ProfileUpdateForm(instance=UserInfo.objects.get(user_id=u_pk))
        context = {
            'u_form': u_form,
            'p_form': p_form
        }
        return render(request, 'users/user_update.html', context)


def register(request):
    if not request.user.has_perm('webapp.can_register'):
        raise PermissionDenied("You don't have permission!")
    else:
        if request.method == 'POST':
            form = UserForm(request.POST)
            p_form = UserInfoForm(request.POST)
            if form.is_valid() and p_form.is_valid():
                user = form.save()
                user.save()
                profile = p_form.save(commit=False)
                profile.user_id = user.pk
                profile.save()
                group = form.cleaned_data.pop('groups')
                child = p_form.cleaned_data.pop('child')
                for user_child in child:
                    profile.child.add(user_child)
                for user_group in group:
                    user.groups.add(user_group)
                username = form.cleaned_data.get('username')
                messages.success(request, f'Пользователь {username} успешно зарегистрирован')
                return redirect('webapp:user_detail', pk=profile.pk)
            else:
                messages.info(request, f'Ошибка регистрации')
        else:
            form = UserForm()
            p_form = UserInfoForm()
        return render(request, 'users/registration.html', {'form': form, 'p_form': p_form})


class UserDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = UserInfo
    template_name = 'users/user_detail.html'
    permission_required = 'webapp.view_userinfo'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now_pk = int(self.kwargs.get('pk'))
        current_user = UserInfo.objects.get(id=now_pk)
        context['current_user'] = current_user
        return context


class UserListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = UserInfo
    template_name = 'users/user_list.html'
    permission_required = 'webapp.view_userinfo'

    def get_queryset(self):
        queryset = UserInfo.objects.filter(user__is_active=True, user__is_staff=False)
        return queryset


def soft_delete_user(request, pk):
    user = get_object_or_404(User, pk=pk)
    user.is_active = False
    user.save()
    return redirect('webapp:users_list')
