from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from webapp.models import Program, UserInfo


class MainListView(LoginRequiredMixin, ListView):
    model = Program
    template_name = 'main_views/main.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        userinfo = UserInfo.objects.all()
        context['userinfo'] = userinfo
        return context
