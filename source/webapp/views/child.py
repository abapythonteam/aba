import re
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect, render
from webapp.models import Child, Test, UserInfo, UsersChild
from django.views.generic import ListView, DetailView, CreateView, UpdateView, View
from webapp.forms import ChildForm
from django.urls import reverse
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


def same(self):
    if self.request.user.is_superuser or str(self.request.user.groups.values_list('name', flat=True)[0]) == "ACD":
        return Child.objects.filter(is_deleted=False)
    else:
        now_user = UserInfo.objects.get(id=self.request.user.client.pk)
        user_child = UsersChild.objects.filter(user_id=now_user.pk)
        childs_pk = re.findall(r'\d+', str(user_child.values('child_id')))
        return Child.objects.filter(id__in=list(map(int, childs_pk)), is_deleted=False)


class ChildList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    queryset = Child.objects.active()
    model = Child
    template_name = 'child_views/child_list.html'
    permission_required = 'webapp.view_child'

    def get_queryset(self):
        return same(self)


class ChildDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Child
    template_name = 'child_views/child_detail.html'
    permission_required = 'webapp.view_child'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['test_list'] = Test.objects.filter(child_id=self.object.pk)
        return context

    def get_queryset(self):
        return same(self)


class ChildUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Child
    template_name = 'child_views/child_update.html'
    form_class = ChildForm
    permission_required = 'webapp.change_child'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('webapp.change_child'):
            raise PermissionDenied("You don't have a permission to edit child")
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('webapp:child_detail', kwargs={'pk': self.object.pk})


class ChildCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Child
    template_name = 'child_views/child_create.html'
    form_class = ChildForm
    permission_required = 'webapp.add_child'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('webapp.add_child'):
            raise PermissionDenied("You don't have a permission to create child")
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if not self.request.user.is_superuser and not str(
                self.request.user.groups.values_list('name', flat=True)[0]) == "ACD":
            self.user_indent()
        return reverse('webapp:child_detail', kwargs={'pk': self.object.pk})

    def user_indent(self):
        UsersChild.objects.create(child_id=self.object.pk, user_id=self.request.user.pk)


def soft_delete_child(request, pk):
    if not request.user.has_perm('webapp.delete_child'):
        raise PermissionDenied("You don't have permission to delete child")
    else:
        child = get_object_or_404(Child, pk=pk)
        child.is_deleted = True
        child.save()
        return redirect('webapp:child_list')


class ChildSearchView(LoginRequiredMixin, View):
    template_name = 'child_views/child_search_results.html'

    def get(self, request):
        query = self.request.GET.get('q')
        searched_child = Child.objects.filter(
            Q(first_name__icontains=query) |
            Q(last_name__icontains=query) |
            Q(third_name__icontains=query) |
            Q(characteristic__icontains=query))
        context = {
            'searched_child': searched_child
        }
        return render(self.request, self.template_name, context)
