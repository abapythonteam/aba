from django.shortcuts import render


def error_view(request):
    return render(request, 'error_views/error.html')


def instruction(request):
    return render(request, 'error_views/instruction.html')
