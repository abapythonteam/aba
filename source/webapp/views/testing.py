from django.core.exceptions import PermissionDenied
from django.shortcuts import reverse
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import RedirectView, TemplateView
from webapp.models import Test, TestResult, Skill, Child, UserInfo, Categories


class CreateTesting(LoginRequiredMixin, PermissionRequiredMixin, RedirectView):
    permission_required = 'webapp.can_test'

    def __init__(self, ):
        super(RedirectView, self).__init__()
        self.test = None

    def create_test(self, child_pk):
        child = Child.objects.get(pk=child_pk)
        user = self.request.user
        attending_therapist = UserInfo.objects.get(user=user)
        self.test = Test.objects.create(child=child, attending_therapist=attending_therapist)

    def create_test_results(self):
        all_skills = Skill.objects.all()
        for skill in all_skills:
            TestResult.objects.create(test=self.test, skill=skill)

    def get_redirect_url(self, child_pk):
        self.create_test(child_pk)
        self.create_test_results()
        return reverse('webapp:start_testing', kwargs={'child_pk': child_pk, 'test_pk': self.test.pk})


class StartTesting(LoginRequiredMixin, TemplateView):
    template_name = 'testing/testing_process.html'

    @staticmethod
    def sort_by_category(test):
        results_by_category = {}
        categories = Categories.objects.all()
        results = TestResult.objects.filter(test=test)
        for cat in categories:
            category_skills = results.filter(skill__category=cat)
            results_by_category[cat.name] = category_skills
        return results_by_category

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_testing = Test.objects.get(pk=kwargs.get('test_pk'))
        context['results'] = self.sort_by_category(current_testing)
        context['test_info'] = current_testing
        return context


@csrf_exempt
def update_result_level(request, pk):
    if not request.user.has_perm('webapp.can_test'):
        raise PermissionDenied("You don't have permission!")
    else:
        result = TestResult.objects.get(pk=pk)
        counter = int(request.POST.get('value', None))
        if request.user and result.status and result.test.status:
            if result.level == counter:
                result.level = result.level - 1
            else:
                result.level = counter
            result.save()
        return JsonResponse({'level': result.level})


@csrf_exempt
def update_result_status(request, pk):
    if not request.user.has_perm('webapp.can_test'):
        raise PermissionDenied("You don't have permission!")
    else:
        result = TestResult.objects.get(pk=pk)
        if request.user and result.test.status:
            result.status = not result.status
        result.save()
        return JsonResponse({'status': result.status})


@csrf_exempt
def update_test_status(request, pk):
    if not request.user.has_perm('webapp.can_test'):
        raise PermissionDenied("You don't have permission!")
    else:
        test_status = Test.objects.get(pk=pk)
        if request.user:
            test_status.status = not test_status.status
        test_status.save()
        return JsonResponse({'status': test_status.status})
