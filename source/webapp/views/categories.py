from django.shortcuts import render
from webapp.models import Categories
from django.views.generic import ListView, DetailView, CreateView, UpdateView, View
from webapp.forms import CategoryForm
from django.urls import reverse
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


class CategoriesListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Categories
    template_name = 'category_views/categories_list.html'
    permission_required = 'webapp.view_categories'

    def get_queryset(self):
        return Categories.objects.active()


class CategoriesDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    model = Categories
    template_name = 'category_views/categories_detail.html'
    permission_required = 'webapp.view_categories'


class CategoriesCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Categories
    form_class = CategoryForm
    template_name = 'category_views/categories_create.html'
    permission_required = 'webapp.add_categories'

    def get_success_url(self):
        return reverse('webapp:categories_list')


class CategoriesUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Categories
    template_name = 'category_views/categories_update.html'
    form_class = CategoryForm
    permission_required = 'webapp.change_categories'

    def get_success_url(self):
        return reverse('webapp:categories_list')


class CategoriesSearchView(LoginRequiredMixin, View):
    template_name = 'category_views/categories_search_results.html'

    def get(self, request):
        query = self.request.GET.get('q')
        searched_categories = Categories.objects.filter(
            Q(name__icontains=query) |
            Q(code_category__icontains=query))
        context = {
            'searched_categories': searched_categories
        }
        return render(self.request, self.template_name, context)
