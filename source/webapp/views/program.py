import re
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404, redirect
from webapp.models import Program, Session, Result, SkillsInProgram, Skill, UserInfo, UsersChild, Child
from django.views.generic import ListView, DetailView, CreateView, UpdateView, View
from django.views.generic.edit import FormMixin
from webapp.forms import ProgramForm, SkillsInProgramForm, ProgramCreateForm
from django.urls import reverse
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


def same(self):
    if self.request.user.is_superuser or str(self.request.user.groups.values_list('name', flat=True)[0]) == "ACD":
        return Program.objects.filter(is_deleted=False)
    else:
        now_user = UserInfo.objects.get(id=self.request.user.client.pk)
        user_child = UsersChild.objects.filter(user_id=now_user.pk)
        childs_pk = re.findall(r'\d+', str(user_child.values('child_id')))
        return Program.objects.filter(child__in=list(map(int, childs_pk)), is_deleted=False)


class ProgramListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Program
    template_name = 'program_views/program_list.html'
    permission_required = 'webapp.view_program'

    def get_queryset(self):
        return same(self)


class SkillInProgramUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = SkillsInProgram
    form_class = SkillsInProgramForm
    template_name = 'program_views/program_skill_update.html'
    permission_required = 'webapp.change_skillsinprogram'

    def get_success_url(self):
        return reverse('webapp:program_detail',
                       kwargs={'pk': get_object_or_404(SkillsInProgram, pk=self.kwargs.get('pk')).program.pk})


class AddSkillInProgram(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = SkillsInProgram
    form_class = SkillsInProgramForm
    permission_required = 'webapp.add_skillsinprogram'

    def form_valid(self, form):
        form.instance.program = Program.objects.get(pk=self.kwargs.get('pk'))
        form.instance.status = True
        return super().form_valid(form)

    def get_success_url(self):
        return self.request.POST.get('next', '/')


class ProgramDetailView(LoginRequiredMixin, PermissionRequiredMixin, FormMixin, DetailView):
    model = Program
    template_name = 'program_views/program_detail.html'
    form_class = SkillsInProgramForm
    permission_required = 'webapp.view_program'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now_pk = int(self.kwargs.get('pk'))
        open_skills = {}
        all_session_in_program = Session.objects.filter(program_id=now_pk, status_session=True)
        for session in all_session_in_program:
            all_true_skills = SkillsInProgram.objects.filter(program_id=now_pk, status=True)
            for skill_in_program in all_true_skills:
                current_program_skill = Result.objects.filter(session_id=session.pk, skill_id=skill_in_program)
                for skill_in_result in current_program_skill:
                    current_skill = Skill.objects.filter(id=skill_in_program.skill_id)
                    for skill in current_skill:
                        open_skills[skill.code_skill + " | " + skill.name] = str(skill_in_result.percent)
        context['all_skill'] = open_skills
        return context

    def get_queryset(self):
        return same(self)


class ProgramCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Program
    template_name = 'program_views/program_create.html'
    form_class = ProgramCreateForm
    permission_required = 'webapp.add_program'

    def form_valid(self, form):
        form.instance.author_therapist = self.request.user.client
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('webapp:program_detail', kwargs={'pk': self.object.pk})

    def get_form_kwargs(self):
        kwargs = super(ProgramCreateView, self).get_form_kwargs()
        kwargs['user_pk'] = self.request.user.client.pk
        kwargs['user_is_admin'] = self.request.user.is_superuser
        try:
            kwargs['group'] = str(self.request.user.groups.values_list('name', flat=True)[0])
        except IndexError:
            kwargs['group'] = []
        return kwargs


def soft_delete_program(request, pk):
    if not request.user.has_perm('webapp.delete_program'):
        raise PermissionDenied("You don't have permission to delete program")
    else:
        program = get_object_or_404(Program, pk=pk)
        program.is_deleted = True
        program.save()
        return redirect('webapp:program_list')


class ProgramUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Program
    template_name = 'program_views/program_update.html'
    form_class = ProgramForm
    permission_required = 'webapp.change_program'

    def form_valid(self, form):
        form.instance.program = Program.objects.get(pk=self.kwargs.get('pk'))
        if form.instance.status:
            return super().form_valid(form)

    def get_success_url(self):
        return reverse('webapp:program_detail', kwargs={'pk': self.object.pk})


class ProgramSearchView(LoginRequiredMixin, View):
    template_name = 'program_views/program_search_results.html'

    def get(self, request):
        query = self.request.GET.get('q')
        searched_program = Child.objects.filter(
            Q(first_name__icontains=query) |
            Q(last_name__icontains=query) |
            Q(third_name__icontains=query))
        context = {
            'searched_program': searched_program
        }
        return render(self.request, self.template_name, context)
