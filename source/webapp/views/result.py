from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect
from webapp.models import Program, Session, Result, SkillsInProgram
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from webapp.views.session import automatic_session_closure


class ResultListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = Result
    template_name = 'session_views/session_result.html'
    permission_required = 'webapp.view_result'

    def get(self, request, *args, **kwargs):
        session = get_object_or_404(Session, pk=self.kwargs['pk'])
        response = super().get(request, *args, **kwargs)
        if request.COOKIES.get("session_number") and session.pk == int(request.COOKIES.get("session_number")):
            session = get_object_or_404(Session, pk=self.kwargs['pk'])
            session.status_session = True
            session.save()
            response.delete_cookie("session_number")
            response.delete_cookie("program_number")
            return response
        else:
            return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now_pk = int(self.kwargs.get('pk'))
        context['session_pk'] = now_pk
        current_session = Session.objects.get(id=now_pk)
        context['status_session'] = current_session.status_session
        context['date_of_session'] = current_session.created_date
        context['terapist'] = current_session.attending_therapist
        current_program = Program.objects.get(id=current_session.program_id)
        context['child'] = current_program.child
        return context

    def get_queryset(self):
        now_session = Result.objects.filter(session=self.kwargs['pk'])
        for result in now_session:
            if result.total == 0:
                empty_result = Result.objects.get(pk=result.pk)
                empty_result.delete()
        return Result.objects.filter(session=self.kwargs['pk'])


def change_status_skill(request, pk):
    if not request.user.has_perm('webapp.can_move_skill_in_close'):
        raise PermissionDenied("You don't have permission!")
    else:
        result = get_object_or_404(SkillsInProgram, pk=pk)
        session_pk = request.GET.get('session')
        result.status = False
        result.save()
        return redirect('webapp:session_result_view', pk=session_pk)


def close_session_result_view(request, pk):
    session = get_object_or_404(Session, pk=pk)
    automatic_session_closure()
    return redirect('webapp:program_detail', pk=session.program_id)
