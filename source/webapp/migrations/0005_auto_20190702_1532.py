# Generated by Django 2.2 on 2019-07-02 09:32

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('webapp', '0004_child_communication_system'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userinfo',
            name='phone',
            field=models.CharField(default=996, max_length=50, verbose_name='Телефон пользователя'),
            preserve_default=False,
        ),
    ]
