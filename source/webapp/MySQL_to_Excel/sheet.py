import os
import xlsxwriter
import datetime
from django.http.response import HttpResponse
from webapp.models import Program, Session, Result, SkillsInProgram
from django.conf import settings


class ProgramData(object):
    def __init__(self, program_pk):
        self.program = Program.objects.get(pk=program_pk)
        self.skills = SkillsInProgram.objects.get_queryset().filter(program=self.program)
        self.sessions = Session.objects.get_queryset().filter(program=self.program)
        self.results_by_skill = {}
        self.collect_data()

    def collect_data(self):
        for skill in self.skills:
            key ='%s %s' % (skill.skill.code_skill, skill.skill.name)
            results = []
            for session in self.sessions:
                try:
                    Result.objects.get(session=session, skill=skill)
                except:
                    pass
                else:
                    results.append(Result.objects.get(session=session, skill=skill))
            if len(results) != 0:
                self.results_by_skill[key] = results


class DataToExcel(object):
    def __init__(self, data):
        self.file_name = settings.TEMP_DIR + 'chart_line.xlsx'
        self.workbook = xlsxwriter.Workbook(self.file_name)
        self.worksheet = self.workbook.add_worksheet()
        self.chart = None
        self.headings = ['Дата', 'Номер сессии', 'С подсказкой', 'Без подсказки', 'Общее', 'Процент']
        self.bold = self.workbook.add_format({'bold': 1})
        self.position = 7
        self.data = data

    def compose_book(self):
        self.worksheet.set_column(0, len(self.data.sessions), 14)
        self.worksheet.set_row(0, None, self.bold)
        self.add_info()
        for key in self.data.results_by_skill:
            self.add_table(key)
            self.add_chart(key, {1: 'red', 2: 'blue'}, self.position + 8)
            self.add_chart(key, {2: 'blue', 3: 'green'}, self.position + 23)
            self.add_chart(key, {4: 'black'}, self.position + 38)
            self.position += 53
        self.workbook.close()

    def add_info(self):
        child_name = '%s %s' % (self.data.program.child.first_name, self.data.program.child.last_name)
        self.worksheet.write_column(0, 0, ['Имя ребенка', child_name])
        for idx, session in enumerate(self.data.sessions):
            session_number = 'Сессия №%s' % str(idx + 1)
            therapist_name = '%s %s' % (session.attending_therapist.user.first_name,
                                        session.attending_therapist.user.last_name)
            self.worksheet.write_column(0, idx + 1, [session_number, therapist_name])

    def add_table(self, key):
        self.worksheet.write('A' + str(self.position), key, self.bold)
        self.worksheet.write_column('A' + str(self.position + 1), self.headings, self.bold)
        for idx, result in enumerate(self.data.results_by_skill[key]):
            idx += 1
            self.worksheet.write(self.position, idx, result.session.created_date.strftime('%d-%b-%Y')[0: 11])
            self.worksheet.write(self.position + 1, idx, idx)
            self.worksheet.write(self.position + 2, idx, result.done_with_hint)
            self.worksheet.write(self.position + 3, idx, result.done)
            self.worksheet.write(self.position + 4, idx, result.total)
            self.worksheet.write(self.position + 5, idx, result.percent)

    def add_chart(self, session_key, series, chart_position):
        self.chart = self.workbook.add_chart({'type': 'line'})
        for key, value in series.items():
            self.add_series(session_key, key, value)
        self.add_chart_sets(session_key)
        self.worksheet.insert_chart('A' + str(chart_position), self.chart, {'x_offset': 25, 'y_offset': 10})

    def add_series(self, key, series_number, color):
        categories_value_finish = len(self.data.results_by_skill[key])
        name_value_start = self.position + series_number + 1
        categories_start = self.position + 1
        self.chart.add_series({
            'name': ['Sheet1', name_value_start, 0],
            'categories': ['Sheet1', categories_start, 1, categories_start, categories_value_finish],
            'values': ['Sheet1', name_value_start, 1, name_value_start, categories_value_finish],
            'line': {'color': color},
        })

    def add_chart_sets(self, key):
        self.chart.set_title({'name': key})
        self.chart.set_x_axis({'name': 'Сессии'})
        self.chart.set_y_axis({'name': 'Кол-во выполнений'})
        self.chart.set_style(2)


def create_excel_file(request, program_pk):
    program_data = ProgramData(program_pk)
    create_excel = DataToExcel(program_data)
    create_excel.compose_book()
    with open(create_excel.file_name, 'rb') as f:
        response = HttpResponse(f)
        response['Content-Disposition'] = 'attachment; filename=result %s.xlsx' \
                                          % datetime.datetime.now().strftime('%d-%b-%Y')[0: 11]
        response['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        response['Content-Length'] = os.path.getsize(create_excel.file_name)
        return response
