from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.test import Client
from django.test import TestCase
from webapp.models import Skill, Categories, User, Child, UserInfo, Test, TestResult, Program
from django.contrib.auth.models import Group
from webapp.forms import SkillForm, CategoryForm, ChildForm, ProgramForm
from django.urls import reverse


# Program tests------------------------------------------------------------------------------------------------------
class ProgramCreateViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.child = Child.objects.first()
        self.user = User.objects.create_user('john', 'johndoe@gmail.com', 'john_password')
        self.user_info = UserInfo.objects.create(user=self.user, phone=12345)

    def test_program_create_form_valid_data(self):
        form = ProgramForm(data={
            'skills': [Skill.objects.first().pk]
        })
        form_valid = form.is_valid()
        self.assertTrue(form_valid)
        form.instance.author_therapist = self.user_info
        form.instance.child = self.child
        program = form.save()
        self.assertEqual(program.child, self.child)
        self.assertEqual(program.author_therapist, self.user_info)

    def test_program_create_form_blank_data(self):
        form = ProgramForm(data={})
        self.assertTrue(form.is_valid())

    def test_view_correct_temp_and_status_code(self):
        url = reverse('webapp:program_create')
        self.client.login(username=self.user_info.user.username, password='john_password')
        acd_group = Group.objects.get(name="ACD")
        self.user.groups.add(acd_group)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('program_views/program_create.html')

    def test_program_created_correctly_and_redirected(self):
        data = {
            'child': self.child.id,
        }
        url = reverse('webapp:program_create')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProgramUpdateViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.test_child = Child.objects.create(first_name='test_child', last_name='test_child',
                                               age=10, first_parent='test_parent')
        self.user = User.objects.create_user('john', 'johndoe@gmail.com', 'john_password')
        self.user_info = UserInfo.objects.create(user=self.user, phone=123456)
        self.program = Program.objects.first()

    def test_program_update_form_valid_data(self):
        data = {
            'child': self.test_child,
            'author_therapist': self.user_info,
        }
        url = reverse('webapp:program_update', args=(self.program.id,))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


# Testing tests------------------------------------------------------------------------------------------------------
class CreateTestingTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.test_user1 = User.objects.create_user(username='testuser1', password='12345')
        self.user_info = UserInfo.objects.create(user=self.test_user1, phone=12345)
        self.child = Child.objects.first()

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('webapp:create_testing', kwargs={'child_pk': self.child.pk}))
        self.assertRedirects(resp, '/auth/login/?next=/child/1/create_test')

    def test_create_and_redirect_if_logged_in(self):
        self.client.login(username='testuser1', password='12345')
        acd_group = Group.objects.get(name="ACD")
        self.test_user1.groups.add(acd_group)
        resp = self.client.get(reverse('webapp:create_testing', kwargs={'child_pk': self.child.pk}))
        test = Test.objects.first()
        test_results = TestResult.objects.first()
        self.assertEqual(self.client.session['_auth_user_id'], str(self.user_info.user.id))
        self.assertRedirects(resp, '/child/1/test/5')
        self.assertTrue(test.pk == 3)
        self.assertTrue(test_results.test.pk == 3)


class StartTestingViewTest(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.test_user1 = User.objects.create_user(username='testuser1', password='12345')
        self.user_info = UserInfo.objects.create(user=self.test_user1, phone=12345)
        self.child = Child.objects.first()
        self.test = Test.objects.create(child=self.child, attending_therapist=self.user_info)
        self.test_result = TestResult.objects.create(test=self.test, skill_id=1)

    def test_redirect_if_not_logged_in(self):
        resp = self.client.get(reverse('webapp:start_testing',
                                       kwargs={'child_pk': self.child.pk, 'test_pk': self.test.pk}))
        self.assertRedirects(resp, '/auth/login/?next=/child/1/test/%s' % self.test.pk)

    def test_logged_in_uses_correct_template(self):
        self.client.login(username='testuser1', password='12345')
        acd_group = Group.objects.get(name="ACD")
        self.test_user1.groups.add(acd_group)
        resp = self.client.get(reverse('webapp:start_testing',
                                       kwargs={'child_pk': self.child.pk, 'test_pk': self.test.pk}))
        self.assertEqual(self.client.session['_auth_user_id'], str(self.user_info.user.id))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'testing/testing_process.html')

    def test_update_test_status(self):
        self.client.login(username='testuser1', password='12345')
        acd_group = Group.objects.get(name="ACD")
        self.test_user1.groups.add(acd_group)
        resp = self.client.get(reverse('webapp:update_test_status', kwargs={'pk': self.test.pk}))
        self.assertEqual(self.client.session['_auth_user_id'], str(self.user_info.user.id))
        self.assertEqual(resp.status_code, 200)

    def test_update_result_status(self):
        self.client.login(username='testuser1', password='12345')
        acd_group = Group.objects.get(name="ACD")
        self.test_user1.groups.add(acd_group)
        resp = self.client.get(reverse('webapp:update_result_status', kwargs={'pk': self.test_result.pk}))
        self.assertEqual(self.client.session['_auth_user_id'], str(self.user_info.user.id))
        self.assertEqual(resp.status_code, 200)

    def test_update_result_level(self):
        self.client.login(username='testuser1', password='12345')
        acd_group = Group.objects.get(name="ACD")
        self.test_user1.groups.add(acd_group)
        resp = self.client.post(reverse('webapp:update_level', kwargs={'pk': self.test_result.pk}), {'value': '3'})
        self.assertEqual(self.client.session['_auth_user_id'], str(self.user_info.user.id))
        self.assertEqual(resp.status_code, 200)


# Categories tests------------------------------------------------------------------------------------------------------
class CategoriesDetailViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'johndoe@gmail.com', 'john_password')
        self.user_info = UserInfo.objects.create(user=self.user)

    def test_category_exist_and_uses_correct_temp(self):
        created_category = None
        try:
            created_category = Categories.objects.filter().first()
        except ObjectDoesNotExist:
            print('Category does not exist')
        url = reverse('webapp:categories_detail', args=(created_category.id,))
        self.client.login(username=self.user.username, password='john_password')
        acd_group = Group.objects.get(name="ACD")
        self.user.groups.add(acd_group)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'category_views/categories_detail.html')


class CategoriesListViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'johndoe@gmail.com', 'john_password')
        self.user_info = UserInfo.objects.create(user=self.user)

    def test_category_exist_and_uses_correct_temp(self):
        try:
            Categories.objects.filter().first()
        except ObjectDoesNotExist:
            print('Category does not exist')
        url = reverse('webapp:categories_list')
        self.client.login(username=self.user.username, password='john_password')
        acd_group = Group.objects.get(name="ACD")
        self.user.groups.add(acd_group)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class CategoriesCreateViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'johndoe@gmail.com', 'john_password')
        self.user_info = UserInfo.objects.create(user=self.user, phone=12345)

    def test_categories_create_form_valid_data(self):
        form = CategoryForm(data={
            'name': 'Специальные навыки',
            'code_category': 'СП',
        })
        category = form.save()
        self.assertTrue(form.is_valid())
        self.assertEqual(category.name, 'Специальные навыки')
        self.assertEqual(category.code_category, 'СП')

    def test_categories_create_form_blank_data(self):
        form = CategoryForm(data={})
        self.assertFalse(form.is_valid())

    def test_view_correct_temp_and_status_code(self):
        url = reverse('webapp:categories_create')
        self.client.login(username=self.user.username, password='john_password')
        acd_group = Group.objects.get(name="ACD")
        self.user.groups.add(acd_group)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('category_views/categories_create.html')

    def test_skill_created_correctly_and_redirected(self):
        valid_data = {
            'name': 'Специальные навыки',
            'code_category': 'СП'
        }
        url = reverse('webapp:categories_create')
        response = self.client.post(url, valid_data)
        self.assertEqual(response.status_code, 302)


class CategoriesUpdateViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.category = Categories.objects.get(pk=1)

    def test_categories_update_form_valid_data(self):
        valid_data = {
            'name': 'Специальные навыки',
            'code_category': 'СП'
        }
        created_category = None
        try:
            created_category = Categories.objects.filter().first()
        except ObjectDoesNotExist:
            print('Categories does not exist')
        url = reverse('webapp:categories_update', args=(created_category.id,))
        response = self.client.post(url, valid_data)
        self.assertEqual(response.status_code, 302)

# Skill tests-----------------------------------------------------------------------------------------------------------


class SkillCreateViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.category = Categories.objects.get(pk=1)
        self.user = User.objects.create_user('john', 'johndoe@gmail.com', 'john_password')
        self.user_info = UserInfo.objects.create(user=self.user, phone=123456)

    def test_skill_create_form_valid_data(self):
        form = SkillForm(data={
            'code_skill': 'A10',
            'name': 'Брать поощрение',
            'category': 1,
            'skill_comment': 'Комментарий к навыку',
            'description': 'Описание навыка',
            'criterion': 'Критерии ребенка',
            'difficulty': 1

        })
        skill = form.save()
        self.assertTrue(form.is_valid())
        self.assertEqual(skill.code_skill, 'A10')
        self.assertEqual(skill.name, 'Брать поощрение')
        self.assertEqual(skill.skill_comment, 'Комментарий к навыку')
        self.assertEqual(skill.category, self.category)
        self.assertEqual(skill.description, 'Описание навыка')
        self.assertEqual(skill.criterion, 'Критерии ребенка')

    def test_skill_create_form_blank_data(self):
        form = SkillForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'code_skill': ['Обязательное поле.'],
            'name': ['Обязательное поле.'],
            'category': ['Обязательное поле.'],
            'difficulty': ['Обязательное поле.']
        })

    def test_view_correct_temp_and_status_code(self):
        url = reverse('webapp:skill_create')
        self.client.login(username=self.user.username, password='john_password')
        acd_group = Group.objects.get(name="ACD")
        self.user.groups.add(acd_group)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('skill_views/skill_create.html')

    def test_skill_created_correctly_and_redirected(self):
        valid_data = {
            'code_skill': 'A10',
            'name': 'Брать поощрение',
            'category': 1,
            'skill_comment': 'Комментарий к навыку',
            'description': 'Описание навыка',
            'criterion': 'Критерии ребенка',
            'difficulty': 1
        }

        url = reverse('webapp:skill_create')
        response = self.client.post(url, valid_data)
        self.assertEqual(response.status_code, 302)


class SkillUpdateViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.category = Categories.objects.get(pk=1)

    def test_skill_update_form_valid_data(self):

        valid_data = {
            'code_skill': 'A10',
            'name': 'Брать поощрение',
            'category': 1,
            'skill_comment': 'Комментарий к навыку',
            'description': 'Описание навыка',
            'criterion': 'Критерии ребенка',
            'difficulty': 1

        }

        created_skill = None

        try:
            created_skill = Skill.objects.filter().first()
        except ObjectDoesNotExist:
            print('Skill does not exist')

        url = reverse('webapp:skill_update', args=(created_skill.id,))
        response = self.client.post(url, valid_data)
        self.assertEqual(response.status_code, 302)


class SkillDeleteViewTests(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()

    def test_skill_deleted_and_redirected(self):
        created_skill = None

        try:
            created_skill = Skill.objects.filter().first()
        except ObjectDoesNotExist:
            print('Skill does not exist')

        url = reverse('webapp:skill_delete', args=(created_skill.id,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 302)


# Child tests
class ChildCreateViewTest(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('unit', 'unit@gmail.com', 'unit_test')
        self.user_info = UserInfo.objects.create(user=self.user, phone=1234566)

    def test_child_create_form_valid_data(self):
        date = datetime.today().date()
        form = ChildForm(data={
            'first_name': 'Unit',
            'last_name': 'Test',
            'third_name': 'Third',
            'birthday': date,
            'age': '10',
            'address': 'г.Бишкек, мкр. Асанбай, д.48, кв. 18',
            'characteristic': 'Unit test characteristic',
            'preferences': 'Unit test preferences',
            'first_parent': 'Unit test parent',
            'second_parent': 'Unit test second parent',
            'contacts': '0550-86-00-99'
        })
        child = form.save()
        self.assertTrue(form.is_valid())
        self.assertEqual(child.first_name, 'Unit')
        self.assertEqual(child.last_name, 'Test')
        self.assertEqual(child.third_name, 'Third')
        self.assertEqual(child.birthday, date)
        self.assertEqual(child.age, '10')
        self.assertEqual(child.address, 'г.Бишкек, мкр. Асанбай, д.48, кв. 18')
        self.assertEqual(child.characteristic, 'Unit test characteristic')
        self.assertEqual(child.preferences, 'Unit test preferences')
        self.assertEqual(child.first_parent, 'Unit test parent')
        self.assertEqual(child.second_parent, 'Unit test second parent')
        self.assertEqual(child.contacts, '0550-86-00-99')

    def test_child_create_form_blank_data(self):
        form = ChildForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'first_name': ['Обязательное поле.'],
            'last_name': ['Обязательное поле.'],
            'age': ['Обязательное поле.'],
            'first_parent': ['Обязательное поле.'],
        })

    def test_view_correct_temp_and_status_code(self):
        url = reverse('webapp:child_create')
        self.client.login(username=self.user.username, password='unit_test')
        acd_group = Group.objects.get(name="ACD")
        self.user.groups.add(acd_group)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('child_views/child_create.html')

    def test_child_created_correctly_and_redirected(self):
        date = datetime.today().date()
        valid_data = {
            'first_name': 'Unit',
            'last_name': 'Test',
            'third_name': 'Third',
            'birthday': date,
            'age': '10',
            'address': 'г.Бишкек, мкр. Асанбай, д.48, кв. 18',
            'characteristic': 'Unit test characteristic',
            'preferences': 'Unit test preferences',
            'first_parent': 'Unit test parent',
            'second_parent': 'Unit test second parent',
            'contacts': '0550-86-00-99'
        }

        url = reverse('webapp:child_create')
        self.client.login(username=self.user.username, password='unit_test')
        acd_group = Group.objects.get(name="ACD")
        self.user.groups.add(acd_group)
        response = self.client.post(url, valid_data)
        self.assertEqual(response.status_code, 302)


class ChildUpdateViewTest(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_superuser('unit', 'unit@gmail.com', 'unit_test')

    def test_child_update_form_valid_data(self):
        date = datetime.today().date()
        valid_data = {
            'first_name': 'Unit',
            'last_name': 'Test',
            'third_name': 'Third',
            'birthday': date,
            'age': '10',
            'address': 'г.Бишкек, мкр. Асанбай, д.48, кв. 18',
            'characteristic': 'Unit test characteristic',
            'preferences': 'Unit test preferences',
            'first_parent': 'Unit test parent',
            'second_parent': 'Unit test second parent',
            'contacts': '0550-86-00-99'
        }

        created_child = None

        try:
            created_child = Child.objects.filter().first()
        except ObjectDoesNotExist:
            print('Child does not exist')

        url = reverse('webapp:child_update', args=(created_child.id,))
        self.client.login(username=self.user.username, password='unit_test')

        response = self.client.post(url, valid_data)
        self.assertEqual(response.status_code, 302)


class ChildDetailViewTest(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'johndoe@gmail.com', 'john_password')
        self.user_info = UserInfo.objects.create(user=self.user, phone=123456)

    def test_child_exist_and_uses_correct_temp(self):
        created_child = None

        try:
            created_child = Child.objects.filter().first()  # Getting first object from database
        except ObjectDoesNotExist:
            print('Child does not exist')

        url = reverse('webapp:child_detail', args=(created_child.id,))
        self.client.login(username=self.user.username, password='john_password')
        acd_group = Group.objects.get(name="ACD")
        self.user.groups.add(acd_group)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'child_views/child_detail.html')


class ChildDeleteViewTest(TestCase):
    fixtures = ['for_test_dump.json', ]

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_superuser('unit', 'unit@gmail.com', 'unit_test')

    def test_child_deleted_and_redirected(self):
        created_child = None

        try:
            created_child = Child.objects.filter().first()
        except ObjectDoesNotExist:
            print('Child does not exist')

        url = reverse('webapp:child_delete', args=(created_child.id,))
        self.client.login(username=self.user.username, password='unit_test')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 302)
