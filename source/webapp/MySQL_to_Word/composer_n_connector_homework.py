from docx.oxml.ns import nsdecls
from docx.oxml import parse_xml
import collections
from docx import Document
from datetime import datetime
from string import digits


class DocumentViewConnector:
    def __init__(self, gathering_data):
        self.gathering_data = gathering_data

    def compose_doc_file(self, file_name):
        document_composer = WordDocumentComposer(self.gathering_data)
        document_composer.compose_table()
        document_composer.save_document(file_name)


class WordDocumentComposer(DocumentViewConnector):
    def __init__(self, gathering_data):
        super().__init__(gathering_data)
        self.document = Document()
        self.compose_paragraph_data()
        self.table = self.document.add_table(rows=1, cols=3)
        self.save_file_name = 'document.docx'

    def set_filename(self, file_name):
        self.save_file_name = file_name

    def save_document(self, file_name='document,docx'):
        self.document.save(file_name)

    def compose_paragraph_data(self):
        child = self.gathering_data['child']
        date_now = datetime.now().strftime('%d.%m.%Y')
        self.document.add_paragraph('Домашнее задание {}'.format(date_now))
        self.document.add_paragraph('Ребенок: {0} {1}'.format(child.first_name, child.last_name))

    def compose_headers(self):
        headers = ('Навык', 'Пояснение', 'Комментарий')
        for i in range(0, 3):
            self.table.rows[0].cells[i].text = headers[i]

    def compose_sorted_body(self):
        skills_with_categories = {}
        skill_categories = self.gathering_data['skill_categories']
        done_skills = self.gathering_data['done_skills']
        category_skill = []

        for cat_code, cat in skill_categories.items():
            for skill_code, skill in done_skills.items():
                if skill_code.translate(str.maketrans('', '', digits)) == cat_code:
                    category_skill.append(skill_code + ' ' + skill)
                skills_with_categories.update({cat_code + ' ' + cat: category_skill})
            category_skill = []

        return collections.OrderedDict(sorted(skills_with_categories.items()))

    @staticmethod
    def compose_additional_data(current_table, data_arr, index, row_index, col_counter):
        if not data_arr[index]:
            data_arr[index] = ''
        current_table.cell(row_index, col_counter).text = data_arr[index]

    def compose_table(self):
        self.compose_headers()
        sorted_skills_with_categories = self.compose_sorted_body()
        row_counter = 1
        data_index = 0

        for category, skills_list in sorted_skills_with_categories.items():
            sorted_descriptions = collections.OrderedDict(sorted(self.gathering_data['skill_descriptions'].items()))
            sorted_stimulus = collections.OrderedDict(sorted(self.gathering_data['skill_stimulus'].items()))

            self.table.add_row()
            for m_cell in range(0, 2):
                self.table.cell(row_counter, m_cell).merge(self.table.cell(row_counter, m_cell + 1))

            shading_elem = parse_xml(r'<w:shd {} w:fill="AFAFB2"/>'.format(nsdecls('w')))
            self.table.rows[row_counter].cells[0]._tc.get_or_add_tcPr().append(shading_elem)
            self.table.rows[row_counter].cells[0].text = category
            row_counter += 1

            for skill in skills_list:
                self.table.add_row().cells[0].text = '{0}'.format(skill)

                descriptions_list = list(sorted_descriptions.values())
                stimulus_list = list(sorted_stimulus.values())

                self.compose_additional_data(self.table, descriptions_list, data_index, row_counter, col_counter=1)
                self.compose_additional_data(self.table, stimulus_list, data_index, row_counter, col_counter=2)

                data_index += 1
                row_counter += 1
