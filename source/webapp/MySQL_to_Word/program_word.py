import os
from webapp.models import Program, Child, SkillsInProgram
from django.http import HttpResponse, HttpResponseNotFound
from django.conf import settings
from webapp.MySQL_to_Word import composer_n_connector_program


def create_word_program(request, program_pk):
    program = Program.objects.get(pk=program_pk)
    child = Child.objects.get(pk=program.child.pk)
    skills_in_program = SkillsInProgram.objects.filter(program_id=program.pk)

    program_data = {}
    skill_categories = {}
    skill_descriptions = {}
    done_skills = {}
    skill_stimulus = {}

    for skill in skills_in_program:
        skill_descriptions.update({skill.skill.code_skill: skill.skill.description})
        skill_categories.update({skill.skill.category.code_category: skill.skill.category.name})
        done_skills.update({skill.skill.code_skill: skill.skill.name})
        skill_stimulus.update({skill.skill.code_skill: skill.stimulus})

    program_data.update([
        ('child', child),
        ('program', program),
        ('skill_categories', skill_categories),
        ('skill_descriptions', skill_descriptions),
        ('done_skills', done_skills),
        ('skill_stimulus', skill_stimulus)
    ])

    full_file_name = settings.TEMP_DIR + 'child_program.docx'
    connector = composer_n_connector_program.DocumentViewConnector(program_data)
    connector.compose_doc_file(full_file_name)
    response = forming_http_response(full_file_name)
    os.remove(full_file_name)
    return response


def forming_http_response(full_file_name):
    try:
        with open(full_file_name, 'rb') as f:
            file_data = f.read()

        response = HttpResponse(file_data, content_type='application'
                                                        '/vnd.openxmlformats-officedocument.wordprocessingml.document')
        response['Content-Disposition'] = f'attachment; filename=child_program.docx'
        return response

    except IOError:
        response = HttpResponseNotFound('<h1>File does not exist</h1>')
        return response
