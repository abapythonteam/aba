"""source URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import include
from django.conf import settings
from django.conf.urls.static import static
from django.core.exceptions import PermissionDenied
from django.test import SimpleTestCase, override_settings
from django.urls import path


def response_error_handler(request, exception=None):
    return redirect('webapp:error_page')


def permission_denied_view(request):
    raise PermissionDenied


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('webapp.urls')),
    path('auth/', include('django.contrib.auth.urls')),
    path('select2/', include('django_select2.urls')),
    path('403/', permission_denied_view),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler403 = response_error_handler


@override_settings(ROOT_URLCONF=__name__)
class CustomErrorHandlerTests(SimpleTestCase):

    def test_handler_renders_template_response(self):
        response = self.client.get('/403/')
        self.assertContains(response, 'Error handler content', status_code=403)
